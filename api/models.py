# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.
from django.contrib.auth.models import User, Group
from django.conf import settings

from django.dispatch import receiver
from django.db.models.signals import post_save, pre_delete

'''
class UserProfile(models.Model):
	user = models.OneToOneField(User)
	app_last_opened = models.DateTimeField(null=True, blank=True)
	app_version = models.IntegerField(blank = True, null = True)
	country_code = models.CharField(max_length=250)
	country_default_sim_name = models.CharField(max_length=250)
	drive_folder_id = models.CharField(max_length=250)
	email = models.CharField(max_length=250)
	excel_limit = models.CharField(max_length=250)
	firebase_token = models.TextField()
	is_blocked = models.BooleanField(default=False)
	last_login = models.DateTimeField(null=True, blank=True)
	login_device = models.CharField(max_length=250)
	login_device_manufacturer = models.CharField(max_length=250)
	login_device_model = models.CharField(max_length=250)
	login_device_secure_id = models.CharField(max_length=250)
	login_device_serial = models.CharField(max_length=250)
	name = models.CharField(max_length=250)
	paid = models.CharField(max_length=250)
	paid_with = models.CharField(max_length=250)
	pdf_limit = models.CharField(max_length=250)
	subscription_count = models.CharField(max_length=250)
	user_added_count = models.CharField(max_length=250)
	user_photo = models.CharField(max_length=250)
	verified_number = models.CharField(max_length=250)

def create_user_profile(sender, instance, **kwargs):
	if kwargs['created']:
		UserProfile(user=instance).save()

post_save.connect(create_user_profile, sender = User, dispatch_uid = 'create_user_profile_unique')
'''

class UserMaster(models.Model):
	id = models.CharField(max_length=250, primary_key=True)
	user = models.CharField(max_length=250, default=None, blank = True, null = True)
	app_last_opened = models.DateTimeField(null=True, blank=True)
	app_version = models.IntegerField(blank = True, null = True)
	country_code = models.CharField(max_length=250)
	country_default_sim_name = models.CharField(max_length=250)
	drive_folder_id = models.CharField(max_length=250)
	email = models.CharField(max_length=250)
	excel_limit = models.CharField(max_length=250)
	firebase_token = models.TextField()
	is_blocked = models.BooleanField(default=False)
	last_login = models.DateTimeField(null=True, blank=True)
	login_device = models.CharField(max_length=250)
	login_device_manufacturer = models.CharField(max_length=250)
	login_device_model = models.CharField(max_length=250)
	login_device_secure_id = models.CharField(max_length=250)
	login_device_serial = models.CharField(max_length=250)
	name = models.CharField(max_length=250)
	paid = models.CharField(max_length=250)
	paid_with = models.CharField(max_length=250)
	pdf_limit = models.CharField(max_length=250)
	subscription_count = models.CharField(max_length=250)
	user_added_count = models.IntegerField(max_length=250)
	user_photo = models.CharField(max_length=250)
	verified_number = models.CharField(max_length=250,null=True, blank=True)
	app_open_count = models.CharField(max_length=250,blank = True, null = True,default='')

	def __unicode__(self):
		return '%s' % (self.user)

@receiver(post_save, sender=UserMaster)
def usermaster_postsave(sender, instance, **kwargs):
	if kwargs['created']:
		print("created usermaster_postsave")
		print("obj =",instance)
		print(instance.user)
		#post to firebase


class TransactWith(models.Model):
	id = models.CharField(max_length=250, primary_key=True)
	user =models.ForeignKey(
        'UserMaster',
        on_delete=models.CASCADE,
    )
	name = models.CharField(max_length=250)
	number = models.CharField(max_length=250)
	currency = models.CharField(max_length=250)
	remaining_balance = models.DecimalField(max_digits=19, decimal_places=2)
	total_debit = models.DecimalField(max_digits=19, decimal_places=2)
	total_credit = models.DecimalField(max_digits=19, decimal_places=2)
	group_id = models.CharField(max_length=250)
	last_modified_date = models.DateTimeField()
	created_date = models.DateTimeField()
	total_credit_entries = models.IntegerField()
	total_debit_entries = models.IntegerField()

	def __unicode__(self):
		return '%s' % (self.name)

class Debit(models.Model):
	id = models.CharField(max_length=250, primary_key=True)
	user = models.CharField(max_length=250)
	amount = models.CharField(max_length=250)
	date = models.DateField()
	modified_time = models.DateTimeField()
	last_modified_date = models.DateTimeField()
	note = models.TextField()
	username = models.CharField(max_length=250)
	attachment = models.TextField()

	def __unicode__(self):
		return '%s' % (self.id)

class Credit(models.Model):
	id = models.CharField(max_length=250, primary_key=True)
	user = models.CharField(max_length=250)
	amount = models.CharField(max_length=250)
	date = models.DateField()
	modified_time = models.DateTimeField()
	last_modified_date = models.DateTimeField()
	note = models.TextField()
	username = models.CharField(max_length=250)
	attachment = models.TextField()

	def __unicode__(self):
		return '%s' % (self.id)

class DCGroup(models.Model):
	id = models.CharField(max_length=250, primary_key=True)
	user =models.ForeignKey(
        'UserMaster',
        on_delete=models.CASCADE,
    )
	name = models.CharField(max_length=250)
	remaining_balance = models.DecimalField(max_digits=19, decimal_places=2)
	total_credit = models.DecimalField(max_digits=19, decimal_places=2)
	total_debit = models.DecimalField(max_digits=19, decimal_places=2)
	def __unicode__(self):
		return '%s' % (self.id)

class ViolationsOfLimit(models.Model):
	id = models.CharField(max_length=250, primary_key=True)
	user = models.CharField(max_length=250)
	name_entries = models.CharField(max_length=250)
	debitcredit_entries = models.CharField(max_length=250)
	def __unicode__(self):
		return '%s' % (self.id)

class PaymentInfo(models.Model):
	id = models.AutoField(primary_key=True)
	user =models.ForeignKey(
        'UserMaster',
        on_delete=models.CASCADE,
    )
	first_purchase_date = models.DateTimeField(null=True, blank=True)
	last_payment_date = models.DateTimeField(null=True, blank=True)
	last_app_opened = models.DateTimeField(null=True, blank=True)
	app_version = models.IntegerField(blank = True, null = True)
	m_developer_payload = models.TextField()
	google_product_id = models.TextField(default=None, blank = True, null = True)
	m_is_auto_renewing = models.BooleanField(default=False)
	m_item_type = models.CharField(max_length=250)
	m_order_id = models.CharField(max_length=250)
	m_original_json = models.TextField()
	m_package_name = models.CharField(max_length=250)
	m_purchase_state = models.IntegerField()
	m_purchase_time = models.PositiveIntegerField()
	m_signature = models.TextField()
	payment_count = models.CharField(max_length=250)
	payment_type = models.CharField(max_length=250)
	m_token = models.TextField()
	person_email = models.CharField(max_length=250)
	person_name = models.CharField(max_length=250)
	person_number = models.CharField(max_length=250,default="")
	person_country = models.CharField(max_length=250,default=0)
	subscription_expiry_date = models.DateTimeField()
	subscriptions_days_left = models.IntegerField(default=-1)
	is_paid = models.CharField(max_length=250,blank = True, null = True,default='')

	def __unicode__(self):
		return '%s' % (self.id)

class Feedback(models.Model):
	id = models.CharField(max_length=250, primary_key=True)
	user = models.CharField(max_length=250)
	feedback = models.TextField()
	date = models.DateTimeField()
	email = models.CharField(max_length=250)
	username = models.CharField(max_length=250)

	def __unicode__(self):
		return '%s' % (self.id)

class Attachment(models.Model):
	id = models.CharField(max_length=250, primary_key=True)
	attachment_id = models.TextField()
	attachment_name = models.TextField()
	attachment_type = models.CharField(max_length=250)
	attachment_public_link = models.TextField()
	attachment_file_size = models.CharField(max_length=250)

	def __unicode__(self):
		return '%s' % (self.id)
