from django.conf.urls import url, include
from api import views
from rest_framework.routers import DefaultRouter


router = DefaultRouter()


urlpatterns = [
	url(r'^updation-data/$', views.UpdationData.as_view()),

	#after sync successfully done. Copy below url for other models and change url and class name
	url(r'^user-master-updation/$', views.UserMasterUpdation.as_view()),
	url(r'^payment-updation/$', views.PaymentsInfoUpdation.as_view()),
	url(r'^groups-updation/$', views.GroupsClass.as_view()),
	url(r'^transactWith-updation/$', views.TransactWithUpdationClass.as_view()),
	url(r'^clean_up/$', views.CleanUpData.as_view()),
	url(r'^fetch_user/$', views.LoadUserData.as_view()),
	url(r'^fetch_transactwith/$', views.LoadTransactWithData.as_view()),
	url(r'^fetch_groups/$', views.LoadGroupsData.as_view()),
	url(r'^fetch_credit_debit/$', views.LoadCreditDebitData.as_view()),

]
