# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from api.models import *

from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin

from django.utils.html import format_html

from django.contrib import messages

from django.utils import timezone

from rangefilter.filter import DateRangeFilter, DateTimeRangeFilter
'''
class UserProfileInline(admin.StackedInline):
	model = UserProfile

admin.site.unregister(User)

class UserNewAdmin(ExportMixin, UserAdmin):
	inlines = [UserProfileInline, ]
	list_display = ('id', 'username', 'name', 'date_joined', 'last_login', 'is_staff', 'email')
	list_filter = ('date_joined', 'last_login', 'is_staff', 'groups', )
	search_fields = ('id', 'username', 'userprofile__name', 'groups__name', 'date_joined', 'last_login', 'is_staff', 'email', )

	ordering = ('-id',)

	#resource_class = UserResource

	def get_queryset(self, request):
	    return super(UserNewAdmin, self).get_queryset(request).select_related('userprofile').prefetch_related('groups')

	def name(self, obj):
		try:
			name = obj.userprofile.name
			return name
		except:
			return ""

def user_unicode(self):
    #return  u'%s, %s' % (self.last_name, self.first_name)
    return  '%s %s - %s' % (self.first_name, self.last_name, self.username)

User.__unicode__ = user_unicode

admin.site.register(User, UserNewAdmin)



class UserProfileAdmin(ExportMixin, admin.ModelAdmin):
	list_display = ('id', 'user', 'email', 'app_last_opened', 'app_version', 'country_code', 'is_blocked', )
	list_filter = ('country_code', 'is_blocked', 'last_login')
	search_fields = ('id', 'user__username', 'email', 'app_last_opened', 'app_version', 'country_code', 'is_blocked', )
	raw_id_fields = ('user',)

	def get_queryset(self, request):
	    return super(UserProfileAdmin, self).get_queryset(request).select_related('user')

admin.site.register(UserProfile, UserProfileAdmin)
'''

name = models.CharField(max_length=250)
paid = models.CharField(max_length=250)
paid_with = models.CharField(max_length=250)
pdf_limit = models.CharField(max_length=250)
subscription_count = models.CharField(max_length=250)
user_added_count = models.CharField(max_length=250)
user_photo = models.CharField(max_length=250)
verified_number = models.CharField(max_length=250,null=True, blank=True)



class UserMasterAdmin(admin.ModelAdmin):
	list_display = ('id', 'email','app_last_opened', 'app_open_count','app_version', 'country_code', 'is_blocked', 'paid','paid_with','subscription_count','user_added_count','verified_number')
	list_filter = ('country_code', 'is_blocked', ('app_last_opened', DateRangeFilter),'last_login','user_added_count','paid','paid_with', 'app_version', 'is_blocked')
	search_fields = ('id', 'user', 'email', 'name', 'app_last_opened', 'app_version', 'country_code', 'is_blocked', 'last_login','paid','paid_with','subscription_count','user_added_count','verified_number')

	def save_model(self, request, obj, form, change):
		obj.save()

		user = form.cleaned_data['user']
		email = form.cleaned_data['email']

		print("UserMasterAdmin save_model")
		print("obj =",obj)
		print(obj.user)
		#post to firebase

admin.site.register(UserMaster, UserMasterAdmin)



class TransactWithAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'name', 'number', 'currency', 'remaining_balance', 'total_debit', 'total_credit', 'created_date', 'total_credit_entries', 'total_debit_entries')
	list_filter = ('currency', 'created_date', 'last_modified_date')
	search_fields = ('id', 'user', 'name', 'number', 'currency', 'remaining_balance', 'total_debit', 'total_credit', 'created_date' )

admin.site.register(TransactWith, TransactWithAdmin)



class DebitAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'username', 'amount', 'date', 'modified_time', 'last_modified_date', 'note')
	list_filter = ('date', 'modified_time', 'last_modified_date')
	search_fields = ('id', 'user', 'amount', 'date', 'modified_time', 'last_modified_date', 'note', 'username')

admin.site.register(Debit, DebitAdmin)



class CreditAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'username', 'amount', 'date', 'modified_time', 'last_modified_date', 'note')
	list_filter = ('date', 'modified_time', 'last_modified_date')
	search_fields = ('id', 'user', 'amount', 'date', 'modified_time', 'last_modified_date', 'note', 'username')

admin.site.register(Credit, CreditAdmin)



class DCGroupAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'name', 'remaining_balance', 'total_credit', 'total_debit')
	#list_filter = ('date', 'modified_time', 'last_modified_date')
	search_fields = ('id', 'user', 'name', 'remaining_balance', 'total_credit', 'total_debit')

admin.site.register(DCGroup, DCGroupAdmin)



class ViolationsOfLimitAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'name_entries', 'debitcredit_entries')
	#list_filter = ('date', 'modified_time', 'last_modified_date')
	search_fields = ('id', 'user', 'name_entries', 'debitcredit_entries')

admin.site.register(ViolationsOfLimit, ViolationsOfLimitAdmin)



class PaymentInfoAdmin(admin.ModelAdmin):
	list_display = ('user','is_paid','last_app_opened', 'subscription_expiry_date', 'm_order_id','google_product_id','payment_type', 'person_email','subscriptions_days_left', 'person_number','person_country','app_version','m_is_auto_renewing')
	list_filter = ('last_app_opened','is_paid','first_purchase_date', 'last_payment_date','subscription_expiry_date','google_product_id','payment_type','person_country','app_version','last_app_opened','m_is_auto_renewing','subscriptions_days_left','person_country')
	search_fields = ( 'user','is_paid', 'first_purchase_date', 'subscription_expiry_date', 'm_order_id',  'person_email', 'person_name')

admin.site.register(PaymentInfo, PaymentInfoAdmin)



class FeedbackAdmin(admin.ModelAdmin):
	list_display = ('id', 'user', 'feedback', 'date', 'email', 'username')
	list_filter = ('date', )
	search_fields = ('id', 'user', 'feedback', 'date', 'email', 'username')

admin.site.register(Feedback, FeedbackAdmin)



class AttachmentAdmin(admin.ModelAdmin):
	list_display = ('id', 'attachment_id', 'attachment_name', 'attachment_type', 'attachment_public_link', 'attachment_file_size')
	list_filter = ('attachment_type', )
	search_fields = ('id', 'attachment_id', 'attachment_name', 'attachment_type', 'attachment_public_link', 'attachment_file_size')

admin.site.register(Attachment, AttachmentAdmin)
