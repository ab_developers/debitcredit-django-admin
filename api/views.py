# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render

from api.models import *

from pprint import pprint

from rest_framework import generics, permissions, viewsets, status
from rest_framework.decorators import api_view, permission_classes, detail_route, list_route

from rest_framework.response import Response

from rest_framework.views import APIView

from django.db.models import Case, When

from datetime import datetime

import requests
import json

from django import db
import ijson
from io import StringIO 
# Create your views here.

from dateutil.parser import parse

import datetime
import dateinfer
from datetime import datetime, timedelta
from dateutil.relativedelta import *

from django.contrib.staticfiles.storage import staticfiles_storage
from urllib.request import urlopen
import os
import decimal

class CleanUpData(APIView):
	permission_classes = (permissions.AllowAny,)

	def get(self, request, format=None):
		
		#6 months old date
		date = datetime.now()
		six_months_old_date = date + relativedelta(months=-6)

		f = open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/deleted_users+'+str(datetime.now())+'+.json','w')
		with open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/main_initial_data.json', 'r') as data:
			objects = ijson.items(data, 'users')
			deleted_users = []
			for users in objects:
				for key, value	in users.items():
					try:
						if value['app_last_opened'] != "":
							app_last_opened = datetime.strptime(value['app_last_opened'], '%a, %b %d,%Y %I:%M %p')
							if app_last_opened < six_months_old_date:
								#f.write('\n'.join((key,json.dumps(value),'\n')))
								deleted_users.append({
									key: value
								})
					except Exception as e:
						print(key,e)

		f.write(json.dumps(deleted_users))

		#THIS SHOW HOW THE PARSER METHOD PARSE
		#print(list(ijson.parse(StringIO('{"users":{"C30ue1RPndeHiIWijJGms5w6Dzo1":{"app_last_opened":"Sat, Jul 21,2018  08:16 PM","app_version":33,"country_code":"971","country_default_sim_name":"AE","drive_folder_id":"1ycHp7J1g_2m9gBo85menRlFgiB0ZB_8o","email":"abumoallim16@gmail.com","excel_limit":"5","firebase_token":"f6zrImZRlSE:APA91bG6eoJ4dQRwVndDyVXfGuGU3hX21D2aX2dcs3TLKpaGyRggGvKnQeHmcx6jWxMgQ84yCNLO_DW0uiyF8jfm7k6o9oz-qgL5uhHWHBLBjA8nysqfD5NdKhm_OXP_wBV-9wpO4IQYlEzQxC9u-MJNw5KnKPK4Zw","is_blocked":false,"last_login":"Sat, Jul 21,2018  08:03 PM","login_device":"j7y17lte","login_device_manufacturer":"samsung","login_device_model":"SM-J730GM","login_device_secure_id":"android_id","login_device_serial":"520075d4e817740d","name":"Abubakker Moallim","pdf_limit":"5","user_added_count":2,"user_photo":"https://lh3.googleusercontent.com/-2zQDgvdt-Hc/AAAAAAAAAAI/AAAAAAAAAuc/dcQCgZAsNfQ/photo.jpg","verified_number":"+919638816816"}}}'))))

		#updation logic end
		return Response({"success": "Data loaded successfully"})

def decimal_default(obj):
    if isinstance(obj, decimal.Decimal):
        return str(obj)
    raise TypeError("Object of type '%s' is not JSON serializable" % type(obj).__name__)


class LoadUserData(APIView):
	permission_classes = (permissions.AllowAny,)

	def get(self, request, format=None):

		user = []
		transactWith = []
		transactions = []

		id = request.GET['id']

		f = open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/user_+'+id+'+.json','wb')
		with open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/main_initial_data.json', 'r') as data:
			
			#Getting User Data
			objects = ijson.items(data, 'users')
			for users in objects:
				for key, value	in users.items():
					if key == id:
						user.append({ key : value })
						break

			#f.write(json.dumps(user))
			f.write(json.dumps(user,default=decimal_default,ensure_ascii=False).encode('utf8'))
			#f.write(json.dumps(transactWith),"\n")

		#updation logic end
		return Response({"success": "Data loaded successfully"})

class LoadTransactWithData(APIView):
	permission_classes = (permissions.AllowAny,)

	def get(self, request, format=None):

		user = []
		transactWith = []
		transactions = []

		id = request.GET['id']

		f = open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/transactWith_+'+id+'+.json','wb')
		with open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/main_initial_data.json', 'r') as data:
			
			
			#Gettting TranactWith
			objects = ijson.items(data, 'transactWith')
			for transactWithObject in objects:
				for key, value in transactWithObject.items():
					if key == id:
						transactWith.append({ key : value })
						break
						#print(transactWith)


			#f.write(json.dumps(user))
			f.write(json.dumps(transactWith,default=decimal_default,ensure_ascii=False).encode('utf8'))
			#f.write(json.dumps(transactWith),"\n")

		#updation logic end
		return Response({"success": "Data loaded successfully"})

class LoadCreditDebitData(APIView):
	permission_classes = (permissions.AllowAny,)

	def get(self, request, format=None):


		creditList = []
		debitList = []

		transactWithList = []

		id = request.GET['id']

		#load transactWith
		json_data = open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/transactWith_+'+id+'+.json')   
		TransactWith = json.load(json_data)
		allNames = TransactWith[0][id]

		for key, value in allNames.items():
			transactWithList.append(key)

		print(transactWithList)

		credit = open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/credit_+'+id+'+.json','wb')
		debit = open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/debit_+'+id+'+.json','wb')
		with open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/main_initial_data.json', 'r') as data:
			
			
			#Gettting TranactWith
			objects = ijson.items(data, 'transactions')

			for creditObjects in objects:
				for key, value in creditObjects.items():
					if key == "credit":
						for key, value in value.items():
							if key in transactWithList:
								creditList.append({ key : value })
					if key == "debit":
						for key, value in value.items():
							if key in transactWithList:
								debitList.append({ key : value })

			credit.write(json.dumps(creditList,default=decimal_default,ensure_ascii=False).encode('utf8'))
			debit.write(json.dumps(debitList,default=decimal_default,ensure_ascii=False).encode('utf8'))

			#f.write(json.dumps(transactWith),"\n")"""

		#updation logic end
		return Response({"success": "Data loaded successfully"})

class LoadGroupsData(APIView):
	permission_classes = (permissions.AllowAny,)

	def get(self, request, format=None):

		groups = []

		id = request.GET['id']

		f = open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/groups_+'+id+'+.json','wb')
		with open('/home/abubakker/djangoProjects/debitcredit/dbtcrt/static/main_initial_data.json', 'r') as data:
			
			
			#Gettting TranactWith
			objects = ijson.items(data, 'groups')
			for groupObject in objects:
				for key, value in groupObject.items():
					if key == id:
						groups.append({ key : value })
						break
						#print(transactWith)


			#f.write(json.dumps(user))
			f.write(json.dumps(groups,default=decimal_default,ensure_ascii=False).encode('utf8'))
			#f.write(json.dumps(transactWith),"\n")

		#updation logic end
		return Response({"success": "Data loaded successfully"})



class UpdationData(APIView):
	permission_classes = (permissions.AllowAny,)

	def get(self, request, format=None):
		#updation logic start


		#get data from firebase
		#you may need to delete your whole local data to re-update your database
		url = "http://firebase.com"
		jsonarr = {}
		headers = {"Content-Type": "application/json"}
		r = requests.get(url, data=json.dumps(jsonarr), headers=headers)

		#r contains response from firebase
		data_arr = r.json()
		#or
		#data_arr = r.text


		for data in data_arr:
			print("data ==",data)

			username = data["username"] #{"username":"abu"}
			email = data["email"] #{"email":"abu@mail.com"}
			first_name = data["name"]["firstname"] #for sub json value. ex. {"name":{"firstname":"abu"}}


			app_last_opened = data["app_last_opened"]
			app_version = data["app_version"]
			country_code = data["country_code"]
			#..get and update UserProfile according you get data from firebase json


			#username should be unique always
			user = User.objects.create(username=username, email=email)
			user.first_name=first_name
			user.save()
			#your user data should be store inti UserProfile because User table is already exists in Django and you can't modify
			UserProfile.objects.filter(user=user).update(app_last_opened=app_last_opened, app_version=app_version, country_code=country_code)
			#..get and update UserProfile according you get data from firebase json


			TransactWith.objects.create(user=user, name=data["name"], number=data["number"]) #set key, value as you get from server...
			Debit.objects.create(user=user, amount=data["amount"], date=parse(data["date"]))	#use parse to convert any date format to django date format
			Credit.objects.create(user=user, amount=data["amount"], date=parse(data["date"]))

			#may be need to use for loop
			DCGroup.objects.create(user=user, name=data["name"])

			ViolationsOfLimit.objects.create(user=user, name_entries=data["name_entries"])

			PaymentInfo.objects.create(user=user, first_purchase_date=parse(data["first_purchase_date"]))

			Attachment.objects.create(user=user, attachment_id=data["attachment_id"])

			Feedback.objects.create(user=user, date=parse(data["date"]))

		#updation logic end
		return Response({"success": "Data updated successfully"})


#after sync successfully done. Copy below class for other models and change classname, model name and fields. test and change according your json data
class UserMasterUpdation(APIView):
	permission_classes = (permissions.AllowAny,)

	def get(self, request, format=None):
		#get data from firebase
		#you may need to delete your whole local data to re-update your database

		UserMaster.objects.all().delete()

		#use POSTMAN to check get data from firebase
		url = "https://debitcredit-7ecc0.firebaseio.com/users.json/?auth=xk0L5sC4g0RymRPMOLG6EY4NuAit925h5s73O28z&orderBy=%22app_last_opened%22"
		jsonarr = {}
		headers = {
			#"Authorization": "Token fdshfjkhfjdhsfkjdhskfhshfkjshfdshfdshfdshf",
			"Content-Type": "application/json"
			}
		r = requests.get(url, data=json.dumps(jsonarr), headers=headers)

		#or if no "data" json
		#r = requests.get(url, headers=headers)

		#r contains response from firebase
		data_arr = r.json()
		#or
		#data_arr = r.text

		#print "data_arr =",data_arr

		# '''
		# for key in data_arr.keys():
		# 	#in key = "BYXp2vNPTodQXDyMeotkgmFqcUf1"
		# 	user_id=key
		# 	data = data_arr[user_id]
		# 	umObj = UserMaster.objects.create(id=user_id, user=user_id,........... )
		# '''

		dictList = []
		for data in data_arr:
			#this is Key that is UserID
			#print "data ==",data

			#this is Object of UserID
			#print "inner object==",data_arr[data]

			userObject = data_arr[data]
			last_login = None;
			if userObject.get("last_login","") == '':
				print(" empty last_login value =========",userObject.get("last_login",""))
			else :
				last_login = datetime.strptime(userObject.get("last_login",""), '%a, %b %d,%Y %I:%M %p')

			app_last_opened = None;
			if userObject.get("app_last_opened","") == '':
				print(" empty last_login value =========",userObject.get("app_last_opened",""))
			else :
				app_last_opened = datetime.strptime(userObject.get("app_last_opened",""), '%a, %b %d,%Y %I:%M %p')
				#print "last_app_opened",userObject.get("app_last_opened","")

			user_added_count = 0
			if userObject.get("user_added_count",0) == "":
				user_added_count = 0
			else:
				user_added_count = userObject.get("user_added_count",0)
			
			app_version = 0
			if userObject.get("app_version",0) == "":
				app_version = 0
			else:
				app_version = userObject.get("app_version",0)


			is_blocked = False
			if userObject.get("is_blocked",False) == "":
				is_blocked = False
			else:
				is_blocked = userObject.get("is_blocked",False)


			print(userObject)

			Obj= UserMaster(
				id=data,
				user=data,
				app_last_opened=app_last_opened,
				app_open_count=userObject.get("app_open_count",'0'),
				app_version=app_version,
				country_code=userObject.get("country_code",""),
				country_default_sim_name=userObject.get("country_default_sim_name",""),
				drive_folder_id=userObject.get("drive_folder_id",""),
				email=userObject.get("email",""),
				excel_limit=userObject.get("excel_limit",5),
				firebase_token=userObject.get("firebase_token",""),
				is_blocked=is_blocked,
				last_login=last_login,
				login_device=userObject.get("login_device",""),
				login_device_manufacturer=userObject.get("login_device_manufacturer",""),
				login_device_model=userObject.get("login_device_model",""),
				login_device_secure_id=userObject.get("login_device_secure_id",""),
				login_device_serial=userObject.get("login_device_serial",""),
				name=userObject.get("name",""),
				paid=userObject.get("paid",False),
				paid_with=userObject.get("paid_with",""),
				pdf_limit=userObject.get("pdf_limit",5),
				subscription_count=userObject.get("subscription_count",""),
				user_added_count=user_added_count,
				user_photo=userObject.get("user_photo",""),
				verified_number=userObject.get("verified_number","")
			)
			dictList.append(Obj)

			try:
					'''umObj = UserMaster.objects.create(
					id=data,
					user=data,
					app_last_opened=app_last_opened,
					app_version=userObject.get("app_version",0),
					country_code=userObject.get("country_code",""),
					country_default_sim_name=userObject.get("country_default_sim_name",""),
					drive_folder_id=userObject.get("drive_folder_id",""),
					email=userObject.get("email",""),
					excel_limit=userObject.get("excel_limit",5),
					firebase_token=userObject.get("firebase_token",""),
					is_blocked=userObject.get("is_blocked",False),
					last_login=last_login,
					login_device=userObject.get("login_device",""),
					login_device_manufacturer=userObject.get("login_device_manufacturer",""),
					login_device_model=userObject.get("login_device_model",""),
					login_device_secure_id=userObject.get("login_device_secure_id",""),
					login_device_serial=userObject.get("login_device_serial",""),
					name=userObject.get("name",""),
					paid=userObject.get("paid",False),
					paid_with=userObject.get("paid_with",""),
					pdf_limit=userObject.get("pdf_limit",5),
					subscription_count=userObject.get("subscription_count",""),
					user_added_count=userObject.get("user_added_count",0),
					user_photo=userObject.get("user_photo",""),
					verified_number=userObject.get("verified_number",""))'''

			except Exception as e: print(e)
		
		UserMaster.objects.bulk_create(dictList)
		return Response("Success")



class PaymentsInfoUpdation(APIView):
			permission_classes = (permissions.AllowAny,)

			def get(self, request, format=None):
				#get data from firebase
				#you may need to delete your whole local data to re-update your database
				PaymentInfo.objects.all().delete()

				#use POSTMAN to check get data from firebase
				url = "https://debitcredit-7ecc0.firebaseio.com/payments_info.json/?auth=xk0L5sC4g0RymRPMOLG6EY4NuAit925h5s73O28z"
				jsonarr = {}
				headers = {
					#"Authorization": "Token fdshfjkhfjdhsfkjdhskfhshfkjshfdshfdshfdshf",
					"Content-Type": "application/json"
					}
				r = requests.get(url, data=json.dumps(jsonarr), headers=headers)

				#or if no "data" json
				#r = requests.get(url, headers=headers)

				#r contains response from firebase
				data_arr = r.json()
				#or
				#data_arr = r.text

				#print "data_arr =",data_arr

				# '''
				# for key in data_arr.keys():
				# 	#in key = "BYXp2vNPTodQXDyMeotkgmFqcUf1"
				# 	user_id=key
				# 	data = data_arr[user_id]
				# 	umObj = UserMaster.objects.create(id=user_id, user=user_id,........... )
				# '''

				count = 0 ;
				for data in data_arr:

					#this is Key that is UserID
					#print "data ==",data
					userObject = data_arr[data]
					#print "object=============",userObject

					first_purchase_date = None;
					if userObject.get("first_purchase_date","") == '':
						print(" empty first_purchase_date value =========",userObject.get("first_purchase_date",""))
					else :
						first_purchase_date = datetime.strptime(userObject.get("first_purchase_date",""), '%a, %b %d,%Y %I:%M %p')

					last_payment_date = None;
					if userObject.get("last_payment_date","") == '':
						print(" empty last_payment_date value =========",userObject.get("last_payment_date",""))
					else :
						last_payment_date = datetime.strptime(userObject.get("last_payment_date",""), '%a, %b %d,%Y %I:%M %p')


					subscription_expiry_date = None;
					if userObject.get("subscription_expiry_date","") == '':
						print(" empty subscription_expiry_date value =========",userObject.get("subscription_expiry_date",""))
					else :
						subscription_expiry_date = datetime.strptime(userObject.get("subscription_expiry_date",""), '%a, %b %d,%Y %I:%M %p')


					person_country = None;
					try:
						user = UserMaster.objects.filter(pk=data)[0]
						#print "UserObject1============",user.country_code
						person_country = user.country_code
						#print "Country============",person_country
						#print person_country
					except Exception as e:	person_country = None

					last_app_opened = None;
					try:
						user = UserMaster.objects.filter(pk=data)[0]
						last_app_opened = user.app_last_opened
					except Exception as e:	last_app_opened = None

					app_version = None;
					try:
						user = UserMaster.objects.filter(pk=data)[0]
						app_version = user.app_version
					except Exception as e:	app_version = None

					person_number = None;
					try:
						user =  UserMaster.objects.filter(pk=data)[0]
						#print "UserObject2============",user.verified_number
						person_number = user.verified_number
						#print person_number
					except Exception as e:	person_number = None

					type = None;
					#print "orderId======",userObject.get("mOrderId","")
					#print "orderId======",userObject.get("mOrderId","").startswith('GPA-')

					if userObject.get("mOrderId","").startswith('GPA.'):
						payment_type="Google"
					else :
						payment_type="PayUMoney"

					try:
						orderJson = json.loads(userObject.get("mOriginalJson"))
						google_product_id = orderJson.get("productId")
					except Exception as e: google_product_id = ""


					#print "subscription_expiry_date",subscription_expiry_date
					#print "last_payment_date",last_payment_date
					#print "first_purchase_date",first_purchase_date

					try:
						umObj = PaymentInfo.objects.update_or_create(
						user=user,
						first_purchase_date=first_purchase_date,
						last_payment_date=last_payment_date,
						last_app_opened=last_app_opened,
						app_version=app_version,
						m_developer_payload=userObject.get("mDeveloperPayload",""),
						m_is_auto_renewing=userObject.get("mIsAutoRenewing",True),
						m_item_type=userObject.get("mItemType",""),
						m_order_id=userObject.get("mOrderId",""),
						m_original_json=userObject.get("mOriginalJson",""),
						m_package_name=userObject.get("mPackageName",""),
						m_purchase_state=userObject.get("mPurchaseState",0),
						m_purchase_time=userObject.get("m_purchase_time",0),
						m_signature=userObject.get("mSignature",""),
						payment_count=userObject.get("payment_count",""),
						payment_type=userObject.get("payment_type",""),
						m_token=userObject.get("mToken",""),
						person_email=userObject.get("personEmail",""),
						person_name=userObject.get("personName",""),
						subscriptions_days_left=userObject.get("subscriptions_days_left",-1),
						google_product_id=google_product_id,
						person_country=person_country,
						person_number=person_number,
						subscription_expiry_date=subscription_expiry_date,
						is_paid=userObject.get("paid","null"))
						#print "umObj =================", umObj
						#print "INSERTED ============================ ================="

					except Exception as e: print(e)


					print("COUNT============",count)
					count = count + 1
				return Response({"success": "Payments Info updated successfully"})

class TransactWithUpdationClass(APIView):
			permission_classes = (permissions.AllowAny,)

			def get(self, request, format=None):
				#get data from firebase
				#you may need to delete your whole local data to re-update your database
				#PaymentInfo.objects.all().delete()

				#use POSTMAN to check get data from firebase
				url = "https://debitcredit-7ecc0.firebaseio.com/transactWith.json/?auth=xk0L5sC4g0RymRPMOLG6EY4NuAit925h5s73O28z&orderBy=%22name%22&limitToLast=500"
				jsonarr = {}
				headers = {
					#"Authorization": "Token fdshfjkhfjdhsfkjdhskfhshfkjshfdshfdshfdshf",
					"Content-Type": "application/json"
					}
				r = requests.get(url, data=json.dumps(jsonarr), headers=headers)

				#or if no "data" json
				#r = requests.get(url, headers=headers)

				#r contains response from firebase
				data_arr = r.json()
				#or
				#data_arr = r.text

				#print "data_arr =",data_arr

				# '''
				# for key in data_arr.keys():
				# 	#in key = "BYXp2vNPTodQXDyMeotkgmFqcUf1"
				# 	user_id=key
				# 	data = data_arr[user_id]
				# 	umObj = UserMaster.objects.create(id=user_id, user=user_id,........... )
				# '''

				for data in data_arr:

					#print "====================++STARTING NEW USER ID====================="
					#this is Key that is UserID
					userObject = data_arr[data]
					#print "UserObject=============",userObject

					try:
						user = UserMaster.objects.only('id').get(id=data)
					except Exception as e: user = data

					try:

						for transactWithID in userObject:

							#print "====================++EACH TransactWith IN USER ID====================="

							#print "transactionId===",transactWithID
							transactionObject = userObject[transactWithID]
							#print "transactionObject=============",transactionObject

							created_date = None;
							if transactionObject.get("first_purchase_date","") == '':
								print(" empty last_login value =========",transactionObject.get("created_date",""))
							else :
								created_date = datetime.strptime(transactionObject.get("created_date",""), '%a, %b %d,%Y %I:%M %p')

							last_modified_date = None;
							if transactionObject.get("first_purchase_date","") == '':
								print(" empty last_login value =========",transactionObject.get("last_modified_date",""))
							else :
								last_modified_date = datetime.strptime(transactionObject.get("last_modified_date",""), '%a, %b %d,%Y %I:%M %p')

							try:
								umObj = PaymentInfo.objects.update_or_create(
								id=transactWithID,
								user=data,
								name=transactionObject.get("name",""),
								number= transactionObject.get("number",""),
								currency = transactionObject.get("currency",""),
								remaining_balance = transactionObject.get("remaining_balance",0),
								total_debit = transactionObject.get("total_debit",0),
								total_credit = transactionObject.get("total_credit",0),
								total_credit_entries = transactionObject.get("total_credit_entries",0),
								total_debit_entries = transactionObject.get("total_debit_entries",0),
								last_modified_date = last_modified_date,
								created_date = created_date)

								#print "umObj =================", umObj
								#print "INSERTED ============================ ================="

							except Exception as e: print(e)

					except Exception as e: print(e)

				return Response({"success": "TransactWith Info updated successfully"})


class GroupsClass(APIView):
			permission_classes = (permissions.AllowAny,)

			def get(self, request, format=None):
				#get data from firebase
				#you may need to delete your whole local data to re-update your database
				DCGroup.objects.all().delete()

				#use POSTMAN to check get data from firebase
				url = "https://debitcredit-7ecc0.firebaseio.com/groups.json?auth=xk0L5sC4g0RymRPMOLG6EY4NuAit925h5s73O28z"
				jsonarr = {}
				headers = {
					#"Authorization": "Token fdshfjkhfjdhsfkjdhskfhshfkjshfdshfdshfdshf",
					"Content-Type": "application/json"
					}
				r = requests.get(url, data=json.dumps(jsonarr), headers=headers)

				#or if no "data" json
				#r = requests.get(url, headers=headers)

				#r contains response from firebase
				data_arr = r.json()
				#or
				#data_arr = r.text

				#print "data_arr =",data_arr

				# '''
				# for key in data_arr.keys():
				# 	#in key = "BYXp2vNPTodQXDyMeotkgmFqcUf1"
				# 	user_id=key
				# 	data = data_arr[user_id]
				# 	umObj = UserMaster.objects.create(id=user_id, user=user_id,........... )
				# '''

				count = 0 ;
				for data in data_arr:

					#print "====================++STARTING NEW USER ID====================="
					#this is Key that is UserID
					#print "userId ==",data
					userObject = data_arr[data]
					#print "UserObject=============",userObject

					try:
						user = UserMaster.objects.only('id').get(id=data)
					except Exception as e: user = data

					try:

						for groupObjectData in userObject:

							#print "====================++EACH GROUP IN USER ID====================="

							#print "groupId===",groupObjectData
							groupObject = userObject[groupObjectData]
							#print "GroupObject=============",userObject

							try:
								umObj = DCGroup.objects.create(
								id=groupObjectData,
								user=user,
								name=groupObject.get("name",""),
								remaining_balance = groupObject.get("total_credit",0)-userObject.get("total_debit",0),
								total_debit = groupObject.get("total_debit",0),
								total_credit = groupObject.get("total_credit",0))

								#print "umObj =================", umObj
								#print "INSERTED ============================ ================="

							except Exception as e: print(e)

					except Exception as e: print(e)

				return Response({"success": "Groups Info updated successfully"})
