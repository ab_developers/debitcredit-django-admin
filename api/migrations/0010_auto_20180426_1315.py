# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-04-26 13:15
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0009_auto_20180426_1233'),
    ]

    operations = [
        migrations.AlterField(
            model_name='transactwith',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='api.UserMaster'),
        ),
    ]
