# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-08-29 07:36
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0019_usermaster_app_open_count'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usermaster',
            name='app_open_count',
            field=models.IntegerField(blank=True, default=0, null=True),
        ),
    ]
